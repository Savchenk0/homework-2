import React, { PureComponent } from "react";
import PropTypes from 'prop-types';
class Button extends PureComponent{
       
        render(){
        const {text,backgroundColor,handleClick,isShown} = this.props
        if (isShown){
            return (
            <button onClick ={handleClick}style={{backgroundColor:`${backgroundColor}`}}>{text}</button>
            )
        }
        return null
    }
        
}

Button.propTypes = {
    backgroundColor:PropTypes.string,
    handleClick:PropTypes.func,
    isShown:PropTypes.bool,
    text:PropTypes.string,
}
Button.defaultProps={
    isShown: true,
}
export default Button