import styles from "./Card.module.scss"
import React, { PureComponent } from "react";
import Star from "../Star";
import PropTypes from 'prop-types';

class Card extends PureComponent{
    constructor(){
        super()
        this.state={
            toggleProp:true,
        }
    }
    
    render(){
        const {name,artikul,color,url,price,handleClick} = this.props
      
        return(
            <div className={styles.cardSlot} key={artikul}>
                <img className={styles.cardSlotImage} src={url}/>
                <div className={styles.cardSlotContent}>
             <div className={styles.cardSlotContentNameWrapper}><p className={styles.cardSlotContentName}>{name}</p><Star lsKey={artikul} handleClick={()=>{
                const tempStatus = localStorage.getItem(artikul)
                localStorage.removeItem(artikul)
                if (tempStatus === "false"){
                    localStorage.setItem(artikul,true)
                }
                else{
                    localStorage.setItem(artikul,false)
                }
                }}/></div>
                <p className={styles.cardSlotContentText}>Lorem ipsum dolor sit amet, con adipiscing elit, sed diam nonu.</p>
                <div className={styles.cardSlotContentFooter}><span className={styles.cardSlotContentFooterPrice}>${price}</span> <a onClick={e=>{e.preventDefault() ; handleClick()}}href="#" className={styles.cardSlotContentFooterBtn}>ADD TO CART</a></div>
                </div>
            </div>
        )
    }
}
Card.propTypes = {
    name:PropTypes.string,
    handleClick:PropTypes.func,
    artikul:PropTypes.number,
    color:PropTypes.string,
    url:PropTypes.string,
    price:PropTypes.oneOfType(PropTypes.string,PropTypes.number),
}
Card.defaultProps={
    color:'blue',
    url:"/img/ifNoUrl.jpg",
}

export default Card