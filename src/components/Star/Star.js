import React, {PureComponent} from "react"
import PropTypes from 'prop-types';

class Star extends PureComponent{
    constructor(){
        super()
        this.state={
            toggleProp:true
        }
    }
    render(){
        console.log(this.state)
        const {lsKey,handleClick} = this.props
        if (localStorage.getItem(lsKey) === "true") {
        return (
            <img onClick={()=>{
                handleClick()
                this.setState(state=>({...state,toggleProp:!state.toggleProp}))
            }} src="/img/star-True.svg"/>
        )
    }
        return(
            <img onClick={()=>{
                handleClick()
                this.setState(state=>({...state,toggleProp:!state.toggleProp}))
            }} src="/img/star.svg"/>
        )
    }
}
Star.propTypes = {
  lsKey:PropTypes.number,
  handleClick:PropTypes.func,
}
export default Star