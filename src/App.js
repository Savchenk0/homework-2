import styles from './App.module.scss';
import Button from './components/Button';
import Modal from './components/Modal';
import Card from './components/Card';
import React, { PureComponent } from "react";
import btnStyles from './components/Modal/Modal.module.scss'
import axios from 'axios';



function addCartToLS(){

}
class App extends PureComponent {
  
  constructor(){
    super()
    this.state ={
      isModal1:false,
      isModal2:false,
      productList:[],
      currArtikul:'',
    }
  }

  async componentDidMount(){

    try{
  await axios('/productList.json')
   .then(res =>{
     const {data} = res;
    for (let elem of data){
      const {artikul} = elem
      if (!localStorage.getItem(artikul)){
        localStorage.setItem(artikul,false )
      }
    }
     this.setState((state) =>({...state,productList:res.data}))
  
  })
}
  catch(e){
    console.log(e)
  }  

  }  

   render(){
   console.log(this.state)
   const {isModal1,productList,currArtikul} = this.state

  return (
    <div className={styles.root}>
      <Modal handleClick={()=>this.setState((state)=>({...state,isModal1:false,isModal2:false}))}isVisible={isModal1} header="Do you want to add this to cart?" closeButton={true} text="go for it?" actions={()=>{return <div><a onClick={()=>{
        if (localStorage.getItem(`${currArtikul}Cart`) !== 'true'){
          localStorage.setItem(`${currArtikul}Cart`,'true')
        }
        this.setState(state=>({...state,isModal1:false,isModal2:false}))}}className={btnStyles.modalContentBodyBtn}>Ok</a>
                        <a onClick={()=>this.setState((state)=>({...state,isModal1:false,isModal2:false}))} className={btnStyles.modalContentBodyBtn}>Cancel</a></div>}}/>
       <div className={styles.cardHolder}>

       {productList.map(({name,artikul,url,price}) =><Card handleClick={()=>this.setState((state)=>({...state,isModal1:true,isModal2:false,currArtikul:artikul}))}name={name} artikul={artikul} key={artikul} url={url} price={price}/> )}
       </div>
    </div>
   
  );
    
  
     
       
       
    

  }
}

export default App;
